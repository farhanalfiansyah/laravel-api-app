<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $user = User::all();
        return response()->json(
            $user
        , 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        try {
            $user = User::create($request->all());
            return response()->json([
                "message" => "created data successfully!",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            return response()->json(
                $user
            , 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e
            ], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $userUpdated = $user->update($request->all());
            return response()->json([
                "message" => "updated data successfully!",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json([
                "message" => "deleted data successfully!",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e
            ], 400);
        }
    }
}

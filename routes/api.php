<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('users')->group(function(){
    Route::post('/', [Api\UserController::class, 'store']);
    Route::get('/', [Api\UserController::class, 'getAll']);
    Route::get('/{id}', [Api\UserController::class, 'show']);
    Route::put('/{id}', [Api\UserController::class, 'update']);
    Route::delete('/{id}', [Api\UserController::class, 'destroy']);
});



